module Calculator
  OPERATORS  = %r{\s*([-+/]|\*{1,2})\s*} # match operators in strings like '2 + 3'
  PRECEDENCE = ['**', '*', '/', '+', '-'].freeze

  def eval(expr)
    split = expr.split(OPERATORS) # "2 + 3" => ['2', '+', '3']
    raise 'invalid expression' unless split.length > 2 # derp

    PRECEDENCE.each do |op|
      while index = split.find_index(op) do
        left, right = split.values_at(index-1, index+1).collect(&:to_f)
        reduction   = left.send(op.to_sym, right)

        preceding_redux = index > 1 ? split[0..index-2]  : []
        following_redux = index < split.length - 1 ? split[index+2..-1] : []

        split = preceding_redux + [reduction] + following_redux
      end
    end

    split.first # e.g. the fully reduced value
  end
  module_function :eval
end
