#!/usr/bin/env ruby
# encoding: UTF-8

$: << File.join(File.dirname(__FILE__), '..', 'lib')
require 'calculator'

def handle_input(input)
  result = Calculator.eval(input)
  puts(" => #{result}")
end

repl = -> prompt do
  print prompt
  handle_input(gets.chomp!)
end

puts "Provide an expression to be evaluated immediately..."
puts "Supported operators: ** * / + -"

loop do
  begin
    repl[">> "]
  rescue RuntimeError => e
    puts "⚠ Could not parse expression"
  end
end