require 'spec_helper'

describe Calculator do
  describe '#eval' do
    it 'works per the provided gist' do
      a = Calculator.eval('10 + 2 * 3 / 4')
      expect(a).to eq(11.5)
    end

    describe 'formatting' do
      it 'ignores extra whitespace' do
        a = Calculator.eval(" 10 + 2  \t")
        expect(a).to eq(12)
        b = Calculator.eval("\n    10   +  2  \t\r\n")
        expect(a).to eq(12)
      end
    end

    describe 'basics' do
      it 'adds' do
        a = Calculator.eval('1 + 2')
        expect(a).to eq(3)
        b = Calculator.eval('3 + 4')
        expect(b).to eq(7)
      end

      it 'subtracts' do
        a = Calculator.eval('3 - 2')
        expect(a).to eq(1)
        b = Calculator.eval('3 - 4')
        expect(b).to eq(-1)
      end

      it 'multiplies' do
        a = Calculator.eval('2 * 2')
        expect(a).to eq(4)
        b = Calculator.eval('40 * 500')
      end

      it 'divides' do
        a = Calculator.eval('3 / 2')
        expect(a).to eq(1.5)
        b = Calculator.eval('20 / 6')
        expect(b).to be_within(0.1).of(3.33)
      end

      it 'handles exponents' do
        a = Calculator.eval('3 ** 2')
        expect(a).to eq(9)
      end
    end

    describe 'order of operations' do
      it 'correctly prioritizes exponents' do
        a = Calculator.eval('3 + 3 ** 4')
        expect(a).to eq(84)
        b = Calculator.eval('3 + 2 ** 4')
        expect(b).to eq(19)
        c = Calculator.eval('3 + 3 ** 4 / 2')
        expect(c).to eq(43.5)
      end

      it 'correctly prioritizes multiplication' do
        a = Calculator.eval('2 + 3 * 4')
        expect(a).to eq(14)
        b = Calculator.eval('3 + 2 * 4')
        expect(b).to eq(11)
        c = Calculator.eval('3 + 2 * 4 ** 2')
        expect(c).to eq(35)
      end

      it 'correctly prioritizes division' do
        a = Calculator.eval('2 + 3 / 4')
        expect(a).to eq(2.75)
        b = Calculator.eval('3 + 2 / 4')
        expect(b).to eq(3.5)
      end

      it 'correctly prioritizes addition and subtraction (last)' do
        a = Calculator.eval('2 - 3 / 4')
        expect(a).to eq(1.25)
        b = Calculator.eval('2 + 3 / 4')
        expect(b).to eq(2.75)
        c = Calculator.eval('2 + 3 ** 2 / 4')
        expect(c).to eq(4.25)
      end
    end
  end
end